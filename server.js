const express = require('express');
const http = require('http');
const socketIO = require('socket.io');
const setupSocketRoutes = require('./index');

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

 
setupSocketRoutes(io);

 
const PORT = process.env.PORT || 3000;
server.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
