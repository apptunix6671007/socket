 
function handleMessage(socket) {
    socket.on('message', (message) => {
        console.log('Received message:', message);
     
    });
}
 
function handleJoinChat(socket) {
    socket.on('join', (room) => {
        console.log('User joined room:', room);
        socket.join(room);
       
    });
}

 
function handleLeaveChat(socket) {
    socket.on('leave', (room) => {
        console.log('User left room:', room);
        socket.leave(room);
        
    });
}

 
function setupSocketRoutes(io) {
    io.on('connection', (socket) => {
        console.log('A user connected');

        
        handleMessage(socket);
        handleJoinChat(socket);
        handleLeaveChat(socket);

    });
}

 
module.exports = setupSocketRoutes;
